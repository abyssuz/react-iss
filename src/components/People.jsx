import React from 'react';

const People = ({ people }) => {
  return (
    <div className="people">
      <label>People on ISS, {people.length}</label>
      {people.map((person) => (
        <div className="person" key={person.name}>
          {person.name}
        </div>
      ))}
    </div>
  );
};

export default People;
