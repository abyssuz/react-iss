import React, { useEffect } from 'react';
import { Loader } from '@googlemaps/js-api-loader';

const Map = ({ coordinates }) => {
  const loader = new Loader({
    apiKey: process.env.API_MAP,
    version: 'weekly',
  });
  let map;

  const mapInit = ({ latitude, longitude }) => {
    loader.load().then(() => {
      map = new google.maps.Map(document.querySelector('.map'), {
        center: { lat: +latitude, lng: +longitude },
        zoom: 5,
      });

      new google.maps.Marker({
        position: { lat: +latitude, lng: +longitude },
        map,
        title: 'current ISS location',
      });
    });
  };

  useEffect(() => {
    if (
      coordinates.latitude !== undefined &&
      coordinates.longitude !== undefined
    )
      mapInit(coordinates);
  }, [coordinates]);

  return <div className="map"></div>;
};

export default Map;
