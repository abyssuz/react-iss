import React from 'react';

const Location = ({ coordinates: { longitude, latitude } }) => {
  return (
    <div className="location">
      <label>ISS located at:</label>
      <p>
        longitude: {longitude}, latitude: {latitude}
      </p>
    </div>
  );
};

export default Location;
