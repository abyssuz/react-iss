import React from 'react';

const Time = ({ date: { time, date } }) => {
  return (
    <div className="time">
      <label>Current UTC time: {time}</label>
      <p>{date}</p>
    </div>
  );
};

export default Time;
