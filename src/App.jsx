import React, { useEffect, useState } from 'react';
import Location from './components/Location';
import Map from './components/Map';
import People from './components/People';
import Time from './components/Time';
import './style.css';

const App = () => {
  const [issCoordinates, setCoordinates] = useState({});
  const [issPeople, setPeople] = useState([]);
  const [currentDate, setDate] = useState({});

  const getDate = () => {
    const days = [
      'Sunday',
      'Monday',
      'Thuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ];

    const d = new Date();
    return {
      time: `${d.getUTCHours()}:${d.getUTCMinutes()}`,
      date: `${days[d.getUTCDay()]}, ${d.getUTCDate()}.${
        d.getUTCMonth() + 1
      }.${d.getUTCFullYear()}`,
    };
  };

  const fetchData = async () => {
    const locationResp = await fetch(process.env.API_LOCATION);
    const locationData = await locationResp.json();
    setCoordinates(locationData.iss_position);

    const peopleResp = await fetch(process.env.API_PEOPLE);
    const peopleData = await peopleResp.json();
    setPeople(peopleData.people.filter((person) => person.craft === 'ISS'));

    const date = getDate();
    setDate(date);
  };

  useEffect(() => {
    fetchData();
    let fetchInterval = setInterval(fetchData, 5000);
  }, []);

  return (
    <div className="iss-app">
      <Location coordinates={issCoordinates} />
      <Time date={currentDate} />
      <Map coordinates={issCoordinates} />
      <People people={issPeople} />
    </div>
  );
};

export default App;
